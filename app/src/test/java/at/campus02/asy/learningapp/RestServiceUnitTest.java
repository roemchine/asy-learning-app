package at.campus02.asy.learningapp;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static org.junit.Assert.*;

public class RestServiceUnitTest {

    @Test
    public void getQuestionsTest() throws Exception {
        Response<List<Question>> execute = RestServiceProvider.INSTANCE.getService().getQuestions().execute();

        for (Question question : execute.body()) {
            String questionText = question.getQuestionText();
            assertNotNull(questionText);
            String answerText = question.getAnswerText();
            assertNotNull(answerText);
        }
    }

    @Test
    public void getQuestionTest() throws Exception {
        Response<List<Question>> execute1 = RestServiceProvider.INSTANCE.getService().getQuestions().execute();
        String id = execute1.body().get(0).getQuestionId();

        Response<Question> execute2 = RestServiceProvider.INSTANCE.getService().getQuestion(id).execute();

        assertEquals(id, execute2.body().getQuestionId());
    }

    @Test
    public void getCategoriesTest() throws Exception {
        ArrayList<String> categories = new ArrayList<>();
        categories.add("Allgemeinwissen"); categories.add("Mathe");

        Response<List<String>> execute = RestServiceProvider.INSTANCE.getService().getCategories().execute();

        for (String category : execute.body()) {
            assertTrue(categories.contains(category));
        }
    }

    @Test
    public void getQuestionsOfCategoryTest() throws Exception {
        String category = "Allgemeinwissen";

        Response<List<Question>> execute = RestServiceProvider.INSTANCE.getService().getQuestionsOfCategory(category).execute();

        for (Question question : execute.body()) {
            assertEquals(category, question.getCategory());
        }
    }

    @Test
    public void getQuestionsOfLevelTest() throws Exception {
        int level = 1;

        Response<List<Question>> execute = RestServiceProvider.INSTANCE.getService().getQuestionsOfLevel(level).execute();

        for (Question question : execute.body()) {
            assertEquals(level, question.getLevel());
        }
    }
}