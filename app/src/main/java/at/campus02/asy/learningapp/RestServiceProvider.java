package at.campus02.asy.learningapp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rdorfer on 14.05.16.
 */
public enum RestServiceProvider {

    INSTANCE;

    public static final String BASE_URL = "http://campus02learningapp.azurewebsites.net/";

    private RestService service;

    RestServiceProvider() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

        service = retrofit.create(RestService.class);
    }

    public RestService getService() {
        return  service;
    }

}
