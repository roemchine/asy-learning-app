package at.campus02.asy.learningapp;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rdorfer on 14.05.16.
 */
public class Question implements Parcelable {
    public static final Parcelable.Creator<Question> CREATOR;

    @SerializedName("FrageID")
    private String questionId;

    @SerializedName("Fragetext")
    private String questionText;

    @SerializedName("Antwort")
    private String answerText;

    @SerializedName("Schwierigkeitsgrad")
    private int level;

    @SerializedName("Kategorie")
    private String category;

    @SerializedName("LaengenUndBreitengrad")
    private String longitudeAndLatitude;

    @SerializedName("Bild")
    private String image;

    static {
        CREATOR
                = new Parcelable.Creator<Question>() {
            public Question createFromParcel(Parcel in) {
                return new Gson().fromJson(in.readString(), Question.class);
            }

            public Question[] newArray(int size) {
                return new Question[size];
            }
        };
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public int getLevel() {
        return level;
    }

    public String getAnswerText() {
        return answerText;
    }

    public String getCategory() {
        return category;
    }

    public String getLongitudeAndLatitude() {
        return longitudeAndLatitude;
    }

    public String getImage() {
        return image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(new Gson().toJson(this));
    }
}
