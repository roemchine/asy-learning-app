package at.campus02.asy.learningapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterQuestionsActivity extends AppCompatActivity {

    private SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_question);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }


    public void showAllCategory(View view) throws Exception {
        loadQuestionsByCall( RestServiceProvider.INSTANCE.getService().getQuestions());

    }


    public void showCategory(View view) {
        final ProgressDialog progressDialog = createProgressDialog("Kategorien werden geladen.");
        progressDialog.show();
        RestServiceProvider.INSTANCE.getService().getCategories().enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    List<String> categoryList = response.body();
                    categoryList.removeAll(Collections.singleton(null));
                    final String[] categories = categoryList.toArray(new String[0]);
                    AlertDialog.Builder builder = new AlertDialog.Builder(FilterQuestionsActivity.this);
                    builder.setTitle("Kategorie")
                            .setItems(categories, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String selectedCatagory = categories[which];;
                                    loadQuestionsByCall(RestServiceProvider.INSTANCE.getService().getQuestionsOfCategory(selectedCatagory));
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    String message = response.message();
                    if (message != null) {
                        showHttpError(message);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                progressDialog.dismiss();
                showNoNetworkWarning();
            }
        });


    }

    public void showSurrounding(View view) {

        final LocationManager locationManager = (LocationManager) getApplicationContext()
                .getSystemService(LOCATION_SERVICE);

        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            Toast.makeText(getApplicationContext(), "Standort kann nicht abgefragt werden. Bitte schalten Sie GPS ein.", Toast.LENGTH_LONG).show();
        } else {
            final ProgressDialog progressDialog = createProgressDialog("Standort wird gesucht.");
            progressDialog.show();
            final LocationListener internalLocationListener = new LocationListener() {

                @Override
                public void onLocationChanged(Location location) {
                    progressDialog.dismiss();
                    int gpsCircuit = Integer.parseInt(prefs.getString("gps_circuit", "20"));
                    loadQuestionsByCall(RestServiceProvider.INSTANCE.getService().getQuestionsByCoordinates(location.getLatitude(), location.getLongitude(), gpsCircuit));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            String criteria = null;
            if (isGPSEnabled) {
                criteria = LocationManager.GPS_PROVIDER;
            } else if (isNetworkEnabled) {
                criteria = LocationManager.NETWORK_PROVIDER;
            }

            Looper myLooper = Looper.myLooper();
            locationManager.requestSingleUpdate(criteria, internalLocationListener, myLooper);
            final Handler myHandler = new Handler(myLooper);
            myHandler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Standort konnte nicht gefunden werden.", Toast.LENGTH_LONG).show();
                    locationManager.removeUpdates(internalLocationListener);
                }
            }, 10000);

        }
    }

    private ArrayList<Question> getFilteredQuestions(List<Question> questions) {
        final int maxQuestionsPerRound = Integer.parseInt(prefs.getString("max_questions_per_round", "10"));
        final boolean allLevels = prefs.getBoolean("all_levels", true);
        final int level = Integer.parseInt(prefs.getString("level", "1"));

        ArrayList<Question> filteredQuestions = new ArrayList<Question>();
        int count = 0;
        for (Question question : questions) {
            if (allLevels || (!allLevels && question.getLevel() == level)) {
                count++;
                filteredQuestions.add(question);
                if (count == maxQuestionsPerRound) {
                    break;
                }
            }
        }
        return filteredQuestions;
    }

    private void loadQuestionsByCall(Call<List<Question>> call) {
        final ProgressDialog questionsProgressDialog = createProgressDialog("Fragen werden geladen.");
        questionsProgressDialog.show();
        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                questionsProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    ArrayList<Question> filteredQuestions = getFilteredQuestions(response.body());
                    if (!filteredQuestions.isEmpty()) {
                        Intent showLongitude = new Intent(FilterQuestionsActivity.this, ShowQuestionActivity.class);
                        showLongitude.putParcelableArrayListExtra(ShowQuestionActivity.QUESTIONS_EXTRA, filteredQuestions);
                        startActivity(showLongitude);
                    } else {
                        Toast.makeText(getApplicationContext(), "Zu den ausgewählten Kriterien konnte keine Frage gefunden werden.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    String message = response.message();
                    if (message != null) {
                        showHttpError(message);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                questionsProgressDialog.dismiss();
                showNoNetworkWarning();
            }
        });
    }

    private void showNoNetworkWarning() {
        Toast.makeText(getApplicationContext(), "Netzwerkverbindung nicht möglich. Bitte überprüfen Sie ob Sie mit dem Internet verbunden sind!", Toast.LENGTH_LONG).show();
    }

    private void showHttpError(String errorMessage) {
        Toast.makeText(getApplicationContext(), String.format("Netzwerkverbindung nicht möglich: %s", errorMessage), Toast.LENGTH_LONG).show();
    }

    private ProgressDialog createProgressDialog(String message) {
        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Bitte warten...");
        progress.setMessage(message);
        progress.setCancelable(false);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        return progress;
    }
}
