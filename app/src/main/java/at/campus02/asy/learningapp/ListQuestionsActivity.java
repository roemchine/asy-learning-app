package at.campus02.asy.learningapp;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListQuestionsActivity extends ListActivity {

    public static final String QUESTIONS_EXTRA = "questions_extra";

    private ArrayList<Question> questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        questions = extras.getParcelableArrayList(ListQuestionsActivity.QUESTIONS_EXTRA);

        ArrayList<String> questionTexts = new ArrayList<>();
        for (Question question : questions) {
            questionTexts.add(question.getQuestionText());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ListQuestionsActivity.this,
                android.R.layout.simple_list_item_1, questionTexts);
        setListAdapter(adapter);



    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);


    }
}
