package at.campus02.asy.learningapp;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by rdorfer on 20.05.16.
 */
public class SettingsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
