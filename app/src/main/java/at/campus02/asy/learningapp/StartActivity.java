package at.campus02.asy.learningapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void showQuestions(View view) {
        Intent show_questions = new Intent(this, FilterQuestionsActivity.class);
        startActivity(show_questions);
    }

    public void showSetting(View view) {
        Intent startSettings = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(startSettings);
    }
}
