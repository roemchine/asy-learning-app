package at.campus02.asy.learningapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rdorfer on 14.05.16.
 */
public interface RestService {
    @GET("api/Fragen")
    Call<List<Question>> getQuestions();

    @GET("api/Fragen/{questionId}")
    Call<Question> getQuestion(@Path("questionId") String questionId);

    @GET("api/Kategorie")
    Call<List<String>> getCategories();

    @GET("api/Fragen/Kategorie/{category}")
    Call<List<Question>> getQuestionsOfCategory(@Path("category") String category);

    @GET("api/Fragen/schwierigkeitsgrad/{level}")
    Call<List<Question>> getQuestionsOfLevel(@Path("level") int level);

    @GET("api/Fragen/Koordinaten")
    Call<List<Question>> getQuestionsByCoordinates(@Query("La") double latidude, @Query("Lo") double longitude, @Query("Di") long distance);
}