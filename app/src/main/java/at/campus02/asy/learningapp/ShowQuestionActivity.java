package at.campus02.asy.learningapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ShowQuestionActivity extends AppCompatActivity {

    public static final String QUESTIONS_EXTRA = "questions_extra";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_question);

        Bundle extras = getIntent().getExtras();
        ArrayList<Question> questions = extras.<Question> getParcelableArrayList(ShowQuestionActivity.QUESTIONS_EXTRA);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), questions);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(Question question) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putParcelable(ARG_SECTION_NUMBER, question);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final Question question = getArguments().getParcelable(ARG_SECTION_NUMBER);
            final View rootView = inflater.inflate(R.layout.fragment_show_question, container, false);
            final TextView titleTextView = (TextView) rootView.findViewById(R.id.title_label);
            final TextView questionTextView = (TextView) rootView.findViewById(R.id.question_text_label);
            final ImageView questionImageView = (ImageView) rootView.findViewById(R.id.question_image_view);

            titleTextView.setText(getString(R.string.section_format, question.getCategory(), question.getLevel()));
            questionTextView.setText(question.getQuestionText());

            if (question.getImage() != null && !"".equals(question.getImage())) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            URL url = new URL(question.getImage());
                            final Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            questionImageView.post(new Runnable() {
                                @Override
                                public void run() {
                                    questionImageView.setImageBitmap(bmp);
                                }
                            });

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            //questionImageView.setImageBitmap(null);
                        }
                    }
                }).start();
            } else {
                questionImageView.setImageBitmap(null);
            }

            FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, question.getAnswerText(), Snackbar.LENGTH_LONG) .show();
                }
            });
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final ArrayList<Question> questions;
        private final HashMap<Integer, PlaceholderFragment> fragmentInstances = new HashMap<>();

        public SectionsPagerAdapter(FragmentManager fm, ArrayList<Question> questions) {
            super(fm);
            this.questions = questions;
        }

        @Override
        public Fragment getItem(int position) {
            if (fragmentInstances.get(position) == null) {
                fragmentInstances.put(position, PlaceholderFragment.newInstance(questions.get(position)));
            }
            return fragmentInstances.get(position);
        }

        @Override
        public int getCount() {
            return questions.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }
}
